import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonList,
  IonItem,
  IonText,
  IonAvatar,
  IonLabel,
  IonItemSliding,
  IonItemOption,
  IonItemOptions,
} from "@ionic/react";
import React from "react";
// import ExploreContainer from '../components/ExploreContainer';

const arr = [
  {
    name: "Rey",
    desc: "My name is Rey",
    img:
      "https://i.picsum.photos/id/820/200/300.jpg?hmac=oyShjC6apmZncG0xgz0zZEnh_1_j8eCRZnF8QxQ_PsE",
  },
  {
    name: "Ryan",
    desc: "My name is Ryan",
    img: "https://picsum.photos/id/237/200/300",
  },
  {
    name: "Nanan",
    desc: "My name is Nanan",
    img: "https://picsum.photos/seed/picsum/200/300",
  },
  {
    name: "Gauthier",
    desc: "My name is Gauthier",
    img: "https://picsum.photos/200/300?grayscale",
  },
  {
    name: "Pranay",
    desc: "My name is Pranay",
    img: "https://picsum.photos/200/300/?blur",
  },
  {
    name: "Karthik",
    desc: "My name is Karthik",
    img: "https://picsum.photos/id/870/200/300?grayscale&blur=2",
  },
];
const List: React.FC = () => {
  return (
    <IonList>
      {arr.map((elem) => (
        <IonItemSliding key={elem.name}>
          <IonItem>
            <IonAvatar>
              <img src={elem.img} alt="" />
            </IonAvatar>

            <IonLabel className="ion-padding">
              {" "}
              <h2>{elem.name}</h2>
              <h3>{elem.desc}</h3>
            </IonLabel>
            <IonItemOptions side="end">
              <IonItemOption>Delete</IonItemOption>
            </IonItemOptions>
          </IonItem>
        </IonItemSliding>
      ))}
    </IonList>
  );
};

export default List;
