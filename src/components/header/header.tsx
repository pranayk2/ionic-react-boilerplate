import { IonHeader, IonPage, IonTitle, IonToolbar } from "@ionic/react";
import React from "react";
import classes from "./header.module.scss";

const Header: React.FC = () => {
  return (
    <IonHeader>
      <IonToolbar>
        <IonTitle className={classes.header}>Best App Ever ..!</IonTitle>
      </IonToolbar>
    </IonHeader>
  );
};

export default Header;
