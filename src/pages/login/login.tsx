import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonInput,
  IonButton,
  IonItemDivider,
  IonIcon,
  IonImg,
  IonLabel,
} from "@ionic/react";
import { person, lockClosed, logoFacebook, logoGoogle } from "ionicons/icons";
import React, { useState, useEffect } from "react";
import classes from "./login.module.scss";
// import images
import image from "../../assets/images/logo.png";
import greenTick from "../../assets/images/greenTick.png";
import redCross from "../../assets/images/redCross.png";

const Home: React.FC = () => {
  const [userName, setUserName] = useState("");
  const [userPass, setUserPass] = useState("");
  const [isCorrectUsername, setIsCorrectUsername] = useState("");
  const [isCorrectPassword, setIsCorrectPassword] = useState("");

  const loginUser = () => {
    console.log("abc", userName, userPass);
  };

  const chekUserName = (e: any) => {
    setIsCorrectUsername("false");

    var name = e.target.value;
    var regx = "^[a-zA-Z0-9_]{5,}[a-zA-Z]+[0-9]*$";
    if (name.match(regx)) {
      console.log("matched");
      setIsCorrectUsername("true");
    } else {
      console.log("not Matched");
      setIsCorrectUsername("false");
    }
  };
  const checkPassword = (e: any) => {
    setIsCorrectPassword("false");

    var pass = e.target.value;
    var regx = "^[a-zA-Z0-9_]{5,}[a-zA-Z]+[0-9]*$";
    if (pass.match(regx)) {
      console.log("matched");
      setIsCorrectPassword("true");
    } else {
      console.log("not Matched");
      setIsCorrectPassword("false");
    }
  };
  return (
    <IonPage>
      <IonContent>
        <div className={classes.container}>
          <IonLabel className={classes.title}>Login</IonLabel>
          <IonImg src={image} className={classes.logo} />
          <IonItemDivider className={classes.textField}>
            <IonIcon icon={person} className={classes.icon}></IonIcon>
            <IonInput
              className={classes.input}
              placeholder="UserName"
              onIonChange={chekUserName}
            ></IonInput>
            {console.log("isCorrectUsername", isCorrectUsername)}
            {isCorrectUsername == "true" ? (
              <IonImg src={greenTick} className={classes.validate} />
            ) : isCorrectUsername == "false" ? (
              <IonImg src={redCross} className={classes.validateCross} />
            ) : (
              <span />
            )}
          </IonItemDivider>
          {console.log("isCorrectUsername", isCorrectUsername)}

          <IonItemDivider className={classes.textField}>
            <IonIcon icon={lockClosed}></IonIcon>
            <IonInput
              className={classes.input}
              placeholder="Password"
              type="password"
              onIonChange={checkPassword}
              // onIonChange={(e: any) => setUserPass(e.target.value)}
            ></IonInput>

            {isCorrectPassword == "true" ? (
              <IonImg src={greenTick} className={classes.validate} />
            ) : isCorrectPassword == "false" ? (
              <IonImg src={redCross} className={classes.validateCross} />
            ) : (
              <span />
            )}
          </IonItemDivider>

          <IonButton
            // expand={block}
            shape="round"
            className={classes.buttonLogin}
            onClick={loginUser}
          >
            Login
          </IonButton>
          <IonLabel className={classes.forgotPass}>
            Forgot your password?
          </IonLabel>
          <div className={classes.seperatorView}>
            <hr className={classes.horizontalSeperator} />
            <IonLabel className={classes.orLabel}>OR</IonLabel>
            <hr className={classes.horizontalSeperator} />
          </div>

          <div className={classes.socialDiv}>
            <IonButton
              // expand={block}
              color="tertiary"
              shape="round"
              className={classes.socialButton}
              onClick={loginUser}
            >
              <IonIcon
                icon={logoFacebook}
                className={classes.iconStyle}
              ></IonIcon>
              facebook
            </IonButton>
            <IonButton
              // expand={block}
              color="tertiary"
              shape="round"
              className={classes.socialButton}
              onClick={loginUser}
            >
              <IonIcon
                icon={logoGoogle}
                className={classes.iconStyle}
              ></IonIcon>
              Google
            </IonButton>
          </div>
          <IonLabel className={classes.forgotPass}>
            Dont have account? Sign up
          </IonLabel>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Home;
